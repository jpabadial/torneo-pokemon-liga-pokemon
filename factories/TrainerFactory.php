<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TrainerFactory
 *
 * @author juanc
 */
class TrainerFactory /*implements ITrainerFactory*/ {
    //put your code here
    public static function getTrainer($nombre, $medallas, $edad, $pueblo, $pokemons, $espiritu): \Trainer{
        return new Trainer($nombre, $medallas, $edad, $pueblo, $pokemons, $espiritu);
    }
}
