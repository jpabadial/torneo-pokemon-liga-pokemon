<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PokemonFactory
 *
 * @author juanc
 */
class PokemonFactory /*implements IPokemonFactory*/{
    //put your code here
    public static function getPokemon($nombre,$genero,$tipo,$ataques,$nivel, $vida): Pokemon{
        return new Pokemon($nombre,$genero,$tipo,$ataques,$nivel, $vida);
    }
    
}
