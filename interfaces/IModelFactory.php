<?php

/**
 *
 * @author pabhoz
 */
interface IModelFactory {
    
    public function newPokemon($nombre,$genero,$tipo,$ataques,$nivel): Pokemon;
    public function newTrainer($nombre,$medallas,$edad,$pueblo,$pokemons,$espiritu): Trainer;
    public function newAtaques($nombre,$tipo,$dano,$pp): Ataques;
    public function newTipo($tipo,$fortaleza,$debilidades): Tipo;
    public function newTournament($tipo,$fortaleza,$debilidades): Tournament;
    
    
}
