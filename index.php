<?php

require 'config.php';

function say($what) {
    print "</br>" . $what . "</br>";
    //print "<table>".$what."</table>";
    //print "<tr>".$what."</tr>";
}

//CARGAR DATOS
//TIPOS DE POKEMON 
$tipoElectrico = TipoFactory::getTipo("electrico", ["volador", "agua"], ["tierra", "hierba"]);
$tipoTierra = TipoFactory::getTipo("tierra", ["fuego", "electrico"], ["volador", "hierba"]);
$tipoHierba = TipoFactory::getTipo("hierba", ["roca", "agua"], ["fuego", "dragon"]);
$tipoVolador = TipoFactory::getTipo("volador", ["lucha", "hierba"], ["roca", "electrico"]);
$tipoLucha = TipoFactory::getTipo("lucha", ["normal", null], ["roca", "dragon"]);
$tipoAgua = TipoFactory::getTipo("agua", ["tierra", "roca"], ["hierba", "dragon"]);
$tipoDragon = TipoFactory::getTipo("dragon", ["dragon", null], ["electrico", "roca"]);
$tipoFuego = TipoFactory::getTipo("fuego", ["hierba", "tierra"], ["roca", "fuego"]);
// https://xombitgames.com/2016/08/mejores-pokemon-contra-tipo-agua-veneno-roca 
//ATAQUE POKEMONS
$atkImpactrueno = AtaqueFactory::getAtaque("impactrueno", $tipoElectrico, 15, 25);
$atkTrueno = AtaqueFactory::getAtaque("trueno", $tipoElectrico, 30, 10);
$atkTerremoto = AtaqueFactory::getAtaque("terremoto", $tipoTierra, 20, 16);
$atkFisura = AtaqueFactory::getAtaque("fisura", $tipoTierra, 30, 5);
$atkEspora = AtaqueFactory::getAtaque("espora", $tipoHierba, 25, 10);
$atkDrenadora = AtaqueFactory::getAtaque("drenadora", $tipoHierba, 45, 5);
$atkVuelo = AtaqueFactory::getAtaque("vuelo", $tipoVolador, 20, 15);
$atkVendaval = AtaqueFactory::getAtaque("vendaval", $tipoVolador, 30, 15);
$atkMovimientoSismico = AtaqueFactory::getAtaque("movimiento Sismico", $tipoLucha, 40, 16);
$atkInversion = AtaqueFactory::getAtaque("inversion", $tipoLucha, 60, 24);
$atkMachada = AtaqueFactory::getAtaque("machada", $tipoLucha, 70, 20);
$atkCascada = AtaqueFactory::getAtaque("cascada", $tipoAgua, 45, 10);
$atkHidrobomba = AtaqueFactory::getAtaque("hidrobomba", $tipoAgua, 50, 15);
$atkTorbellino = AtaqueFactory::getAtaque("torbellino", $tipoAgua, 70, 15);
$atkEnfado = AtaqueFactory::getAtaque("enfado", $tipoDragon, 20, 20);
$atkDragoAliento = AtaqueFactory::getAtaque("drago aliento", $tipoDragon, 60, 32);
$atkNucleoCastigo = AtaqueFactory::getAtaque("Nucleo Castigo", $tipoDragon, 90, 35);
$atkLlamarada = AtaqueFactory::getAtaque("llamarada", $tipoFuego, 20, 10);
$atkInfierno = AtaqueFactory::getAtaque("infierno", $tipoFuego, 60, 25);
$atkEstallido = AtaqueFactory::getAtaque("Estallido", $tipoFuego, 50, 20);


$atkRayoSolar = AtaqueFactory::getAtaque("Rayo solar", $tipoHierba, 70, 10);


// CREAR POKEMONS
$PokePicachu = PokemonFactory::getPokemon("Picachu", "M", $tipoElectrico, [$atkImpactrueno, $atkTrueno], 100,100);
$PokeRaichu = PokemonFactory::getPokemon("Raichu", "M", $tipoElectrico, [$atkImpactrueno, $atkTrueno], 100,100);

$PokeOnix = PokemonFactory::getPokemon("Onix", "F", $tipoTierra, [$atkTrueno, $atkTerremoto], 100,100);
$PokeCubone = PokemonFactory::getPokemon("Cubone", "M", $tipoTierra, [$atkTrueno, $atkTerremoto], 100,100);

$PokeBulbasaur = PokemonFactory::getPokemon("Bulbasaur", "F", $tipoHierba, [$atkEspora, $atkDrenadora], 100,100);
$PokeVenusaur = PokemonFactory::getPokemon("Venusaur", "M", $tipoHierba, [$atkRayoSolar, $atkDrenadora], 100,100);

$PokePidgey = PokemonFactory::getPokemon("pidgey", "M", $tipoVolador, [$atkVuelo, $atkVendaval], 100,100);
$PokeGolbat = PokemonFactory::getPokemon("golbat", "F", $tipoVolador, [$atkVuelo, $atkVendaval], 100,100);

$PokeMonkey = PokemonFactory::getPokemon("Monkey", "F", $tipoLucha, [$atkMachada, $atkInversion], 100,100);
$PokeMachamp = PokemonFactory::getPokemon("Machamp", "M", $tipoLucha, [$atkMovimientoSismico, $atkVendaval], 100,100);

$PokeStaryu = PokemonFactory::getPokemon("Staryu", "M", $tipoAgua, [$atkHidrobomba, $atkCascada], 100,100);
$PokeLapras = PokemonFactory::getPokemon("Lapras", "M", $tipoAgua, [$atkMovimientoSismico, $atkTorbellino], 100,100);

$PokeDratini = PokemonFactory::getPokemon("Dratini", "M", $tipoDragon, [$atkEnfado, $atkDragoAliento], 100,100);
$PokeFlygon = PokemonFactory::getPokemon("Lapras", "F", $tipoDragon, [$atkNucleoCastigo, $atkEnfado], 100,100);

$PokeCharmander = PokemonFactory::getPokemon("Charmander", "M", $tipoFuego, [$atkLlamarada, $atkInfierno], 100,100);
$PokeCharmeleon = PokemonFactory::getPokemon("Charmeleon", "M", $tipoFuego, [$atkEstallido, $atkEnfado], 100,100);

$PokePonyta = PokemonFactory::getPokemon("Ponyta", "M", $tipoFuego, [$atkLlamarada, $atkInfierno], 80,100);
$PokeArcanine = PokemonFactory::getPokemon("Arcanine", "M", $tipoFuego, [$atkEstallido, $atkLlamarada], 70,100);
$PokeMagmar = PokemonFactory::getPokemon("Magmar", "M", $tipoFuego, [$atkEstallido, $atkLlamarada], 50,100);





//asignar pokemons a entrenador
//$PEbruk = [$PokePicachu, $PokeRaichu];
//$PEmisty = [$PokePicachu, $PokeRaichu];

$anos = "años";
$meda = "medallas";

//CREAR ENTRENADORES
//$picachu = PokemonFactory::getPokemon("picachu", "M", $tipoElectrico, [$atkImpactrueno, $atkTrueno], 50);

$Ebruk = TrainerFactory::getTrainer("Bruk", 10 . $meda, 22 . $anos, "maleta", [$PokePicachu, $PokeRaichu, $PokeLapras], "Amistoso");
$Emity = TrainerFactory::getTrainer("Misty", 12 . $meda, 18 . $anos, "Cerulean City", [$PokeOnix, $PokeCubone, $PokeLapras], "Amistoso");
$Emax = TrainerFactory::getTrainer("Max", 30 . $meda, 19 . $anos, "valle del cocora", [$PokeBulbasaur, $PokeVenusaur, $PokeFlygon], null);
$EAshKetchum = TrainerFactory::getTrainer("Ash Ketchum", 80 . $meda, 20 . $anos, "Pueblo Paleta", [$PokePidgey, $PokeGolbat, $PokeMonkey], "Competitivo ");
$EGaryOak = TrainerFactory::getTrainer("Gary Oak", 94 . $meda, 22 . $anos, "Shigeru", [$PokeMonkey, $PokeMachamp, $PokeVenusaur], null);
$EDawn = TrainerFactory::getTrainer("Dawn", 27 . $meda, 23 . $anos, "Shigem", [$PokeStaryu, $PokeLapras, $PokeCharmander], null);
$ECilan = TrainerFactory::getTrainer("Cilan", 20 . $meda, 23 . $anos, "Rimen", [$PokeDratini, $PokeFlygon, $PokeVenusaur], null);
$EIris = TrainerFactory::getTrainer("Iris", 20 . $meda, 23 . $anos, "Atanaka", [$PokeCharmander, $PokeCharmeleon, $PokeVenusaur], null);
$EJuanSe = TrainerFactory::getTrainer("Juan Se", 8 . $meda, 20 . $anos, "shishini", [$PokeFlygon, $PokeCharmeleon], null);
$EDionisio = TrainerFactory::getTrainer("Dionisio", 9 . $meda, 19 . $anos, "shis", [$PokeFlygon, $PokeDratini], null);
$EBrayan = TrainerFactory::getTrainer("El Brayan", 2 . $meda, 18 . $anos, "El hueco", [$PokePicachu, $PokeVenusaur], null);
$ESebas = TrainerFactory::getTrainer("Sebas", 9 . $meda, 17 . $anos, "El charco", [$PokePicachu, $PokeCharmeleon], null);
$EJp = TrainerFactory::getTrainer("Jp", 59 . $meda, 47 . $anos, "El Hima", [$PokePonyta, $PokeArcanine,$PokeMagmar], null);




say("TIPO POKEMON->");
say("nombre:");
say($tipoElectrico->getNombre());
say("Fortaleza->");
say($tipoVolador->getFortaleza() [0]);
say($tipoVolador->getFortaleza() [1]);
say("Debilidades->");
say($tipoTierra->getDebilidades() [0]);
say($tipoHierba->getDebilidades() [1]);

say("POKEMON->");
say("Nombre Pokemon:");
say($PokePicachu->getNombre());
say("Genero:");
say($PokePicachu->getGenero());
say("Tipo:");
say($PokePicachu->getTipo()->getNombre());
say("Ataques:");
say($PokePicachu->getAtaques() [0]->getNombre());
say($PokePicachu->getAtaques() [1]->getNombre());
say("Nivel:");
say($PokePicachu->getNivel());

//$picachu = PokemonFactory::getPokemon("picachu", "M", $tipoElectrico, [$atkImpactrueno, $atkTrueno], 50);


say("ENTRENADOR->");
say("Nombre:");
say($Ebruk->getNombre());
say("Medallas:");
say($Ebruk->getMedallas());
say("Edad:");
say($Ebruk->getEdad());
say("Pueblo:");
say($Ebruk->getPueblo());
say("Pokemons:");
say($Ebruk->getPokemons()[0]->getNombre());
say($Ebruk->getPokemons()[1]->getNombre());
say("Tipo Entrenador");
say($Ebruk->getEspiritu());



//CREAR UN AREGLO []
//Y AGREGAS TODOS TRAINERS
//CREAR TORNEO
// INICIAS UN OBJETO DE TIPO TORNEO 
//CREAR TORNEO
$trainerss = [$Ebruk, $Emity, $Emax, $EAshKetchum, $EGaryOak, $EDawn, $ECilan, $EIris, $EJuanSe, $EDionisio, $EBrayan, $ESebas, $EJp];


say("<-PARTICIPANTES VALIDOS->");
$inicioTorneo = new Tournament($nombre, $rondas, $trainerss);
$user = $inicioTorneo->validarParticipante();
foreach ($user as $clave) {
    print_r("Nombre" . ": " . $clave->getNombre() . " || " . $clave->getMedallas() . "</br>");
}
say("<-PERSONAS CON 3 POKEMONES MAXIMO->");
foreach ($user as $clave) {
    $pokemones = $clave->getPokemons();
    foreach ($pokemones as $nombrePokemon) {
        $nombrePokemon->getNombre();
        $pokes = $nombrePokemon->getNombre();
        print_r("Nombre" . ": " . $clave->getNombre() . " || " . "Pokemons: " . " -> " . $pokes . "</br>");
    }
}

say("<-POKEMONES CON NIVEL MAYOR A 100->");

foreach ($user as $clave) {
    $pokemones = $clave->getPokemons();
    foreach ($pokemones as $nombrePokemon) { {
        $pokes = $nombrePokemon->getNombre();
            $nombrePokemon->getNivel();
        }
        print_r(  "Pokemons: " . " -> " . $pokes . " -> ". "Nivel" . ": " . $nombrePokemon->getNivel()."</br>");
    }
}

say("<-BATALLAS POKEMON!!!!!->");
$peleador = $inicioTorneo->parejas();
say($peleador);

say("<-PELEA #1 POKEMON!!!!!->"); 
$contadorsito = 0;
foreach ($user as $clave => $key) {
                $contadorsito +=1;
                switch ($contadorsito) {
                    case 1:
                       $player1 = $key;
                        break;
                    case 2:
                        $player2 = $key;
                        
                        break;
                    case 3:
                        $player3 = $key;
                        break;
                      case 4:
                          $player4 = $key;
                        break;
                }
                
        }
$ssssso = $inicioTorneo->pelear($player1,$player2);
say($ssssso);


say("<-PELEA #2 POKEMON!!!!!->"); 

$ssssso1 = $inicioTorneo->pelear($player3,$player4);
say($ssssso1);










