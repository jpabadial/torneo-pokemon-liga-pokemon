<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tournament
 *
 * @author pabhoz
 */
class Tournament implements IModel {

    //put your code here
    private $nombre;
    private $rondas;
    private $trainer = [];

    public function __construct($nombre, $rondas, $trainer) {
        //parent::__construct();
        $this->nombre = $nombre;
        $this->rondas = $rondas;
        $this->trainer = $trainer;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getRondas() {
        return $this->rondas;
    }

    function getTrainer() {
        return $this->trainer;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setRondas($rondas) {
        $this->rondas = $rondas;
    }

    function setTrainer($trainer) {
        $this->trainer = $trainer;
    }

    function validarParticipante() {
        $entrenadores = $this->trainer;
        $arrame = [];
        $participante = [];
        $count = 0;
        foreach ($entrenadores as $clave => $key) {
            if ($key->getMedallas() >= 10 && sizeof($key->getPokemons()) >= 3) {
                array_push($arrame, $key);
            }
        }

        foreach ($arrame as $pass => $llave) {
            $pokemones = $llave->getPokemons();
            foreach ($pokemones as $nombrePokemon) {

                if ($nombrePokemon->getNivel() >= 100) {
                    $count += 1;
                    if ($count == 3) {
                        array_push($participante, $llave);
                        $count = 0;
                    }
                }
            }
        }
        $this->trainer = $participante;
        return $participante;
    }
    
    
    public function parejas(){
        $entrenadores = $this->trainer;
        $arrame = [];
         $count = 0;
            foreach ($entrenadores as $clave => $key) {
                $count +=1;
                switch ($count) {
                    case 1:
                       $player1 = $key;
                        break;
                    case 2:
                        $player2 = $key;
                        
                        break;
                    case 3:
                        $player3 = $key;
                        break;
                      case 4:
                          $player4 = $key;
                        break;
                }
                
        }
        
        return $player1->getNombre()." vs ".$player2->getNombre()."</br>".$player4->getNombre()." vs ".$player3->getNombre();
    }
    
    public function pelear($player1,$player2){
        $count = 0;
        $count2 = 0;
      
            $pokemones = $player1->getPokemons();
            foreach ($pokemones as $nombrePokemon) {
                $count +=1;
                     switch ($count) {
                    case 1:
                       $poke1 = $nombrePokemon;
                        break;
                    case 2:
                        $poke2 = $nombrePokemon;
                        break;
                    case 3:
                        $poke3 = $nombrePokemon;
                        break;
                }
                
     }
        
        //////// 

            $pokemones2 = $player2->getPokemons();
            foreach ($pokemones2 as $nombrePokemon2) {
                $count2 +=1;
                     switch ($count2) {
                    case 1:
                       $poke21 = $nombrePokemon2;
                        break;
                    case 2:
                        $poke22 = $nombrePokemon2;
                        break;
                    case 3:
                        $poke23 = $nombrePokemon2;
                        break;
       
                }
                
            }
         ////1
        $a = $poke21->getVida()- $poke1->atack();
        $b = $poke1->getVida()- $poke21->atack();
           
        $pelea1 = $poke21->getNombre()."  ATACA A ".$poke1->getNombre()." => ".$poke21->atack()." La vida es =>".$a;
        $pelea12 = $poke1->getNombre()."  ATACA A ".$poke21->getNombre()." => ".$poke1->atack()." La vida es =>".$b;
        
        
        ////2
        $c = $poke22->getVida()- $poke2->atack();
        $d = $poke2->getVida()- $poke22->atack();
           
        $pelea2 = $poke22->getNombre()."  ATACA A ".$poke2->getNombre()." => ".$poke22->atack()." La vida es =>".$c;
        $pelea22 = $poke2->getNombre()."  ATACA A ".$poke22->getNombre()." => ".$poke2->atack()." La vida es =>".$d;
        
        ////3
        $e = $poke23->getVida()- $poke3->atack();
        $f = $poke3->getVida()- $poke23->atack();
           
        $pelea3 = $poke23->getNombre()."  ATACA A ".$poke3->getNombre()." => ".$poke23->atack()." La vida es =>".$c;
        $pelea23 = $poke3->getNombre()."  ATACA A ".$poke23->getNombre()." => ".$poke3->atack()." La vida es =>".$d;
        
       return  $pelea1."</br>".$pelea12. "</br>"."</br>". $pelea2."</br>".$pelea22."</br>"."</br>".$pelea3."</br>".$pelea23."</br>";
      //return "holaaa".$poke1->getNombre();
        
    }

    public function getMyVars() {
        return get_object_vars($this);
    }

}
