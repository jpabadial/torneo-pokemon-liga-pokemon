<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tipo
 *
 * @author juanc
 */
class Tipo {
    //put your code here
    private $nombre;
    private $fortaleza = [];
    private $debilidades = [];
    
        function __construct($nombre,$fortaleza,$debilidades) {
        
        $this->nombre = $nombre;
        $this->fortaleza = $fortaleza;
        $this->debilidades = $debilidades;
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getFortaleza() {
        return $this->fortaleza;
    }

    function getDebilidades() {
        return $this->debilidades;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setFortaleza($fortaleza) {
        $this->fortaleza = $fortaleza;
    }

    function setDebilidades($debilidades) {
        $this->debilidades = $debilidades;
    }


}


