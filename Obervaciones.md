## Observaciones del trabajo

### De los patrones y la estructuración

1. Las fabricas (factories) usadas no cumplen su cometido correctamente, en lugar de tener tantas fábricas deberían haberse centralizado en una fábrica de modelos a menos de que existieran muchos derivados de una entidad específica.
2. Los Bridges usados no son adecuados y no cumplen su cometido.
3. Index es una inicialización de todas las instancias del torneo, pero se supone que dicha inicialización se daría por una UI.
4. No se presentan todas las validaciones y el proyecto carece de métodos, mientras algunos otros se encuentran mla ubicados.
5. El link al video enviado no funcionó.

### De las pruebas

1. Pruebas de unidad para la clase `Attack`:
	- Prueba de creación de ataque <b style="color:red">X</b>
	- Prueba de fortalezas <b style="color:red">X</b>
	- Prueba de debilidades <b style="color:red">X</b>

2. Pruebas de unidad para la clase `Match`:
	- Prueba de creación de un Matchs <b style="color:red">X</b>
	- Prueba de reglas de calculo de daño <b style="color:red">X</b>
	- Prueba de ganador de un encuentro <b style="color:red">X</b>
	- Prueba de trampas en un encuentro <b style="color:red">X</b>
	- Prueba de narración de encuentro <b style="color:red">X</b>
3. Pruebas de unidad para la clase `Pokemon`:
	- Prueba de creación de ataque <b style="color:green">√</b>
	- Validación de tipo de pokemón con sus ataques <b style="color:red">X</b>
	- Validación de capacidad máxima de ataques <b style="color:red">X</b>
4. Pruebas de unidad para la clase `Tournament`:
	- Prueba de creación de Torneo <b style="color:red">X</b>
	- Prueba de creación de rondas <b style="color:red">X</b>
	- Prueba de ganador de ronda <b style="color:red">X</b>
	- Prueba de inscritos <b style="color:red">X</b>
	- prueba de límite de nivel de pokemons inscritos <b style="color:red">X</b>
5. Pruebas de unidad para la clase `Trainer`:
	- Prueba de creación - Prueba de acciones del entrenador por turno <b style="color:red">X</b>
	- Prueba de acciones del entrenador por turno <b style="color:red">X</b>
	- Prueba de límite de pokemons <b style="color:red">X</b>
	- Prueba de probabilidades de acciones por espiritu <b style="color:red">X</b>
6. Pruebas de unidad para la clase `Type`:
	- Prueba de creación <b style="color:red">X</b>
	- Prueba de debilidades <b style="color:red">X</b>
	- prueba de fortalezas <b style="color:red">X</b>

## Calificación
####Implementación y patrones (50%): 3.4
####Desarrollo de pruebas (50%): 0/23 = 0.0 

###Nota final (100%): 1.7